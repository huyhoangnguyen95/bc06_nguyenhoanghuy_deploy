//

var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";

// khi user load trang => lấy dữ liệu từ localStorage

var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  dssv = JSON.parse(jsonData);
  renderDSSV(dssv);
}

function themSinhVien() {
  // lấy thông tin từ form

  var sv = layThongTinTuForm();

  // push()  : thêm phần tử vào array
  dssv.push(sv);

  // convert data
  var dataJson = JSON.stringify(dssv);
  // save to localstorage
  localStorage.setItem(DSSV_LOCAL, dataJson);

  //   render dssv lên table
  renderDSSV(dssv);
  resetForm();
}

function xoaSv(id) {
  console.log("id: ", id);

  //   tìm vị trí id

  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  console.log("viTri: ", viTri);

  if (viTri != -1) {
    dssv.splice(viTri, 1);
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCAL, dataJson);

    var jsonData = localStorage.getItem(DSSV_LOCAL);
    if (jsonData != null) {
      dssv = JSON.parse(jsonData);
      renderDSSV(dssv);
    }
  }
}

// localStorage : lưu trữ , JSON : convert data

function suaSv(id) {
  console.log(id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });

  // show thông tin lên form

  document.getElementById("txtMaSV").value = dssv[viTri].ma;
  document.getElementById("txtTenSV").value = dssv[viTri].ten;
  document.getElementById("txtEmail").value = dssv[viTri].email;
  document.getElementById("txtPass").value = dssv[viTri].matKhau;
  document.getElementById("txtDiemToan").value = dssv[viTri].toan;
  document.getElementById("txtDiemLy").value = dssv[viTri].ly;
  document.getElementById("txtDiemHoa").value = dssv[viTri].hoa;

  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  // lấy thông tin từ form

  var sv = layThongTinTuForm();
  console.log("sv: ", sv);

  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  console.log("viTri: ", viTri);

  dssv[viTri] = sv;
  renderDSSV(dssv);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
