function renderDSSV(dssv) {
  // render dssv lên table
  var contentHTML = "";

  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    contentHTML += `  
      <tr>
          <td> ${item.ma} </td>
          <td> ${item.ten} </td>
          <td> ${item.email} </td>
          <td> 0 </td>
          <td> 
          <button onclick="xoaSv('${item.ma}')" class="btn btn-danger"> Xóa </button>
          </td>
          <td> 
          <button onclick="suaSv('${item.ma}')" class="btn btn-warning"> Sửa </button>
          </td>
      </tr>
      `;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  return {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
}
